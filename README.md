First reactjs full stack app 

### Installation

Project requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies and start the server.

Also you have to create dev.js file inside config in server part
Remember that if you have dynamic IP connection to it can fail, allow all ips
```js
module.exports = {
    mongoURI: '' // your mongoURI token
}
```


```sh
$ <client> npm install
$ <default> npm install
```

For tests, use react scripts see first page...

What it does?

- using reactjs
- using redux
- using backend with nodemailer
- using mongoDB
- administration part and user
- safe passwords
- option for "forgot email"

You can remove some of packages in client, I put them for my future projects on this website start build



