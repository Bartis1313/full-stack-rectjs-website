const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');
const { User } = require("../models/User");

const { auth } = require("../middleware/auth");

//=================================
//             User
//=================================

router.get("/auth", auth, (req, res) => {
    res.status(200).json({
        _id: req.user._id,
        isAdmin: req.user.role === 0 ? false : true,
        isAuth: true,
        email: req.user.email,
        name: req.user.name,
        role: req.user.role,
        image: req.user.image,
    });
});

router.post("/register", (req, res) => {

    const user = new User(req.body);

    user.save((err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).json({
            success: true
        });
    });
});

router.post("/login", (req, res) => {
    User.findOne({ email: req.body.email }, (err, user) => {
        if (!user)
            return res.json({
                loginSuccess: false,
                message: "Auth failed, email not found"
            });

        user.comparePassword(req.body.password, (err, isMatch) => {
            if (!isMatch)
                return res.json({ loginSuccess: false, message: "Wrong password" });

            user.generateToken((err, user) => {
                if (err) return res.status(400).send(err);
                res.cookie("w_authExp", user.tokenExp);
                res
                    .cookie("w_auth", user.token)
                    .status(200)
                    .json({
                        loginSuccess: true, userId: user._id
                    });
            });
        });
    });
});

router.post('/reset', function (req, res) {
    User.findOne({ email: req.body.email }, function (error, userData) {
        let transporter = nodemailer.createTransport({
            host: "",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: '', // generated ethereal user
                pass: '', // generated ethereal password
            },
            tls: {
                rejectUnauthorized: false
            },
        });

        const currentDateTime = new Date();
        const mailOptions = {
            from: '',
            to: req.body.email,
            subject: 'Password Reset',
            html: `<h1>Passport Reset Link</h1><p>\
            <h3>Hello ${userData.name}</h3>\
            If You are requested to reset your password then click on below link<br/>\
            <a href=${location.origin}/change-password/${currentDateTime}+++${userData.email}>Click On This Link</a>\
            </p>`
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
                User.updateOne({ email: userData.email }, {
                    token: currentDateTime,

                }, { multi: true }, function (err, affected,) {
                    if (err) return res.json({ success: false, err });
                    return res.status(200).json({
                        success: true,
                        msg: info.response,
                    });
                })
            }
        });
    })
});

router.post('/updatePassword', (req, res) => { //it's safe, all data is hashed, see User.js
    User.findOne({ email: req.body.email }, (errorFind, userData) => {
        if (userData.token == req.body.linkDate && req.body.password == req.body.confirm_password) {
            let newPassword = req.body.confirm_password;
            let dataForUpdate = { password: newPassword, updatedDate: new Date() };

            User.findOneAndUpdate(condition, dataForUpdate, { new: true }, function (error, updatedUser) {
                if (error) {
                    return res.status(500).json({ msg: 'Server Error', error: error.message });
                } else {               
                    if (!updatedUser) {
                        return res.status(404).json({
                            msg: "User Not Found.",
                            success: false
                        });
                    }
                }
            });
        }
        if (errorFind) return res.json({ success: false, err });
        return res.status(200).send({
            success: true,
            updatedDate: new Date()
        });
    });
})

router.get("/logout", auth, (req, res) => {
    User.findOneAndUpdate({ _id: req.user._id }, { token: "", tokenExp: "" }, (err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).send({
            success: true
        });
    });
});

module.exports = router;

