let admin = (req,res,next) => {
    if(req.user.role === 0 ){
        return res.send('Admin section')
    }
    next()
    //logic: admin is any int higher than 0
}

module.exports = { admin }