import React, { Suspense } from 'react';
import { Route, Switch } from "react-router-dom";
import Auth from "../hoc/auth";
// pages for this product
import LandingPage from "./views/LandingPage/LandingPage.js";
import LoginPage from "./views/LoginPage/LoginPage.js";
import RegisterPage from "./views/RegisterPage/RegisterPage.js";
import NavBar from "./views/NavBar/NavBar";
import Footer from "./views/Footer/Footer";
import NotFoundPage from './views/NotFoundPage/NotFoundPage';
import ForgotPasword from './views/ForgotPassword/ForgotPassword';
import ResetPassword from './views/ForgotPassword/ResetPassword';
//null   Anyone Can go inside
//true   only logged in user can go inside
//true, true only admin can go inside
//false  logged in user can't go inside

function App() {
  return (
    <Suspense fallback={(<div>Loading...</div>)}>
    <div className="wrapper">
      <NavBar />
      <div className="contentsWrapSpacer" />
      <Switch>
          <Route exact path="/" component={Auth(LandingPage, null)} />
          <Route exact path="/login" component={Auth(LoginPage, false)} />
          <Route exact path="/register" component={Auth(RegisterPage, false)} />
          <Route exact path="/forgot-password" component={Auth(ForgotPasword, null)} />
          <Route exact path="/change-password/:slug" component={Auth(ResetPassword, null)} />
        <Route component={Auth(NotFoundPage, null)} />
      </Switch>
      <Footer />
    </div>
  </Suspense>
  );
}

export default App;
