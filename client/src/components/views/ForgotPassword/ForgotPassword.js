import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { resetPassword } from "../../../_actions/user_actions";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form, Icon, Input, Button, Typography } from 'antd';
import { useDispatch } from "react-redux";
import notifier  from 'simple-react-notifications';
import "simple-react-notifications/dist/index.css";
const { Title } = Typography;



//Browser returns a string so anything with || or ? will fail unless you make it a string
let initialEmail = localStorage.getItem("rememberMe");
if (initialEmail === "undefined") initialEmail = '';


function ForgotPassword() {
    const [formErrorMessage, setFormErrorMessage] = useState('');
    const dispatch = useDispatch();

    return (
        <Formik
            initialValues={{
                email: initialEmail,
            }}
            validationSchema={Yup.object().shape({
                email: Yup.string()
                    .email('Email is invalid')
                    .required('Email is required')
            })}
            onSubmit={(values, { setSubmitting }) => {
                notifier.configure({
                    position: "top-right",
                    animation: {
                      in: "fadeIn", // try to comment it out
                      out: "fadeOut",
                      duration: 600 // overriding the default(300ms) value
                    }
                  });
                setTimeout(() => {
                    let dataToSubmit = {
                        email: values.email,
                    };

                    dispatch(resetPassword(dataToSubmit))
                        .then(response => {
                            console.log(response)
                            if (response.payload.success) {
                                notifier.success("Check your email, link is valid for 30 minutes.");
                            } else {
                                notifier.error("Error occured");
                            }
                        })
                        
                        .catch(err => {
                            setFormErrorMessage('Check out your account email once again')
                            setTimeout(() => {
                                setFormErrorMessage("")
                            }, 3000);
                        });
                    setSubmitting(false);
                }, 500);
                
            }}
        >
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                } = props;
                return (
                    <div className="app">

                        <Title level={2}>Forgot Password</Title>
                        <form onSubmit={handleSubmit} style={{ width: '350px' }}>

                            <Form.Item required>
                                <Input
                                    id="email"
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Enter your email"
                                    type="email"
                                    value={values.email}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                        errors.email && touched.email ? 'text-input error' : 'text-input'
                                    }
                                />
                                {errors.email && touched.email && (
                                    <div className="input-feedback">{errors.email}</div>
                                )}
                            </Form.Item>

                            {formErrorMessage && (
                                <label ><p style={{ color: '#ff0000bf', fontSize: '0.7rem', border: '1px solid', padding: '1rem', borderRadius: '10px' }}>{formErrorMessage}</p></label>
                            )}
                            <Form.Item>
                                <div>
                                    <Button type="primary" htmlType="submit" className="login-form-button" style={{ minWidth: '100%' }} disabled={isSubmitting} onSubmit={handleSubmit}>
                                        Reset password
                                    </Button>
                                </div>
                            </Form.Item>
                        </form>
                    </div>
                );
            }}
        </Formik>  
             
    );
};

export default withRouter(ForgotPassword);
