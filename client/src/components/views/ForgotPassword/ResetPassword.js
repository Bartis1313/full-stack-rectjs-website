import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { forgotPassword } from "../../../_actions/user_actions";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form, Input, Button, Typography } from 'antd';
import { useDispatch } from "react-redux";
import notifier  from 'simple-react-notifications';
import "simple-react-notifications/dist/index.css";
const { Title } = Typography;


function ResetPassword(props) {
    const [FormErrorMessage, setFormErrorMessage] = useState('')
    notifier.configure({
        position: "top-right",
        animation: {
          in: "fadeIn", // try to comment it out
          out: "fadeOut",
          duration: 600 // overriding the default(300ms) value
        }
      });
    const dispatch = useDispatch();

    let slugParam = props.match.params.slug;
    let splitSlug = slugParam.split("+++");
    let reqDate = splitSlug[0];
    let email = splitSlug[1];
    let date1 = new Date(reqDate);
    let currentDate = new Date();
    let differenceinMS = currentDate - date1;
    if (differenceinMS > 3600000) {
        notifier.error("Link Not Valid link will be valid for 15 min.Please sent the reset link Again");
        props.history.push("/login");
    }

    return (
        <Formik
            initialValues={{
                password: '',
            }}
            validationSchema={Yup.object().shape({
                password: Yup.string()
                    .min(6, 'Password must be at least 6 characters')
                    .required('Password is required'),
                confirmPassword: Yup.string()
                    .oneOf([Yup.ref('password'), null], 'Passwords must match')
                    .required('Confirm Password is required')
            })}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    let dataToSubmit = {
                        email: email,
                        linkDate: reqDate,
                        confirm_password: values.confirmPassword
                    };

                    dispatch(forgotPassword(dataToSubmit))
                        .then(response => {
                            if (response.payload.success) {
                                console.log(response)
                                notifier.success('Successfully changed password')
                            } else {
                                notifier.error('Check out your Account again')
                            }
                        })
                        .catch(err => {
                            setFormErrorMessage('Check out your Account again')
                            setTimeout(() => {
                                setFormErrorMessage("")
                            }, 3000);
                        });
                    setSubmitting(false);
                }, 500);
            }}
        >
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    dirty,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    handleReset,
                } = props;
                return (
                    <div className="app">

                        <Title level={2}>Password Reset</Title>
                        <form onSubmit={handleSubmit} style={{ width: '350px' }}>

                            <Form.Item required label="Password" hasFeedback validateStatus={errors.password && touched.password ? "error" : 'success'}>
                                <Input
                                    id="password"
                                    placeholder="Enter your password"
                                    type="password"
                                    value={values.password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                        errors.password && touched.password ? 'text-input error' : 'text-input'
                                    }
                                />
                                {errors.password && touched.password && (
                                    <div className="input-feedback">{errors.password}</div>
                                )}
                            </Form.Item>

                            <Form.Item required label="Confirm" hasFeedback>
                                <Input
                                    id="confirmPassword"
                                    placeholder="Enter your confirmPassword"
                                    type="password"
                                    value={values.confirmPassword}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                        errors.confirmPassword && touched.confirmPassword ? 'text-input error' : 'text-input'
                                    }
                                />
                                {errors.confirmPassword && touched.confirmPassword && (
                                    <div className="input-feedback">{errors.confirmPassword}</div>
                                )}
                            </Form.Item>
                            <Form.Item>
                                {/*add some styling for it if you want*/}
                                <div>
                                    <Button type="primary" htmlType="submit" className="login-form-button" style={{ minWidth: '100%' }} disabled={isSubmitting} onSubmit={handleSubmit}>
                                        Reset Password
                                    </Button>
                                </div>
                            </Form.Item>
                        </form>
                    </div>
                );
            }}
        </Formik>
        
    );
};

export default withRouter(ResetPassword);
